// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        mapPrefab:{
          default: null,
          type: cc.Prefab
        },
        nextMap:{
          default: null,
          type: cc.Prefab
        },
        player:{
          default: null,
          type: cc.Sprite
        },
        posx:{
          default:0
        },
        posy:{
          default:0
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      let newMap = cc.instantiate(this.mapPrefab);
      this.node.addChild(newMap);
      newMap.setPosition(cc.v2(0,0));
      newMap.getComponent("PrefabTestMap1").game = this;
    },

    changeNextMap: function(){
      console.log("nextCreate");
      let nextMap=cc.instantiate(this.nextMap);
      this.node.addChild(nextMap);
      nextMap.setPosition(cc.v2(0,0));
    },

    start () {

    },

    // update (dt) {},
});
