// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //豚の移動距離
        moveRange: 48,
        //移動にかかる時間
        moveDuration:0.5,
        //マップのどこにいるのかを示す
        mapPosX: 0,
        mapPosY: 0,
        //アクションが終了したら値が格納されるためアクション終了を認識するためのもの
        actionend: null,
        game:{
          default:null,
          type:cc.Node,
        },
    },

    //--------------
    //移動を実行
    //--------------
    move: function (vector) {
      const now = new Date();
      console.log("move called");
      //moveBy(移動にかかる時間、移動距離).easing(加減速タイプ)easingは使えないみたいだ
      //cc.v2(x,y)移動するベクトル
      const move = cc.moveBy(this.moveDuration,vector);//.easing(cc.easeCubicActionOut);
      if (this.actionend === null) {
        this.actionend = this.node.runAction(move);
        console.log(now.getTime());
        this.scheduleOnce(function() {
          console.log(now.getTime());
          console.log("action finish");
          this.actionend = null;
        }, this.moveDuration);
      }
      console.log(this.actionend);
      //cc.repeatForever　　えいえんにくりかえすよ！
      //return cc.repeatForever(move);
    },



    //--------------------------
    //移動の方向を指定する関数群
    //--------------------------
    setUpMove: function () {
      this.move(cc.v2(0,this.moveRange));
    },
    setDownMove: function () {
      this.move(cc.v2(0,-this.moveRange));
      console.log("change");
      console.log(this.node.SpriteFrame);
      this.node.SpriteFrame = "assets\\Resources\\ground.png";
    },
    setRightMove: function () {
      this.move(cc.v2(this.moveRange,0));
    },
    setLeftMove: function () {
      this.move(cc.v2(-this.moveRange,0));
    },


    //---------------------
    //ノードが破棄されたとき
    //---------------------
    onDestroy: function() {
      //キーイベントの破棄
      cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
      cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },


    //---------------------------------------------------------
    //キーが押されたとき、eventの中にキーの情報なんかが入ってる
    //---------------------------------------------------------
    onKeyDown: function (event) {
      console.log("onKeyDown called");
      switch (event.keyCode) {
        case cc.macro.KEY.a:
          console.log("Press a");
          this.setLeftMove();
          break;
        case cc.macro.KEY.d:
          console.log("Press d");
          this.setRightMove();
          break;
        case cc.macro.KEY.w:
          console.log("Press w");
          this.setUpMove();
          break;
        case cc.macro.KEY.s:
          console.log("Press s");
          this.setDownMove();
          break;
        default:
          break;
      }
    },


    //---------------
    //キーが離された
    //---------------
    onKeyUp: function(event){
    },

    //--------------------------------
    //onLoadはもちろん読み込み時
    //--------------------------------
    onLoad: function(){
      //this.move(cc.v2(this.moveRange,0));
     //キーイベントの受付開始
      cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
      cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    start () {

    },
    //update(dt)  dt毎に処理するよ！
    /*update (dt) {

    },*/
});
