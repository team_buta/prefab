// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        imageNumber:{
          default: null
        },
        eventID:{
          default: null
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      var sprite = new cc.spriteFrame("db://assets/Resources/kusamura.png/kusamura",cc.rect(0,0,90,128));
      console.log(sprite);
    //var sprite = new cc.Sprite();
    //var sprite = spr.create("db://assets/Resources/kusamura.png/kusamura");
    //sprite.x = 20;
    //sprite.y = 20;
    //sprite.spriteFrame = "";

    console.log(sprite.create);
    //console.log(sprite.x);
    //console.log(sprite.spriteFrame);
    //var map = new cc.TMXTiledMap("db://assets/Resources/black.tsx");
    //    this.addChild(map);
    /*const tmx = new cc.TMXTiledMap();
    const map = tmx.create("db://assets/Resources/black.tsx");*/
    //console.log(map);
    //this.node.addChild(sprite, 10);
    },

    nextMap: function(){
      this.game.changeNextMap();
      this.node.destroy();
    },

    getPlayerDistance: function(){
      let playerPos = this.game.player.getPosition();
      let dist = this.node.position.sub(playerPos).mag();
      return dist;
    },

    start () {

    },

    update (dt) {
      if (this.getPlayerDistance() < 20) {
        this.nextMap();
        return;
      }
    },
});
